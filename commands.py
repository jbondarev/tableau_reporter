"""
    -> LoginCommand
    -> GetUrlCommand
    -> DownloadPDFCommand
    -> ConvertToZipCommand
    -> SendToEmailCommand
"""

import pyautogui

import time
import zipfile
import os
import smtplib
import json
import tempfile

from decorators import exc

from email.mime.multipart import MIMEMultipart 
from email.mime.text import MIMEText 
from email.mime.base import MIMEBase 
from email import encoders

from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait, Select

driver = None

@exc('')
def init():
    global driver
    prefs = {
        "download.default_directory" : os.path.dirname(os.path.abspath(__file__)) + "\\reports"
    }

    options = webdriver.ChromeOptions()
    options.add_experimental_option("prefs",prefs)
    options.add_argument("--disable-notifications")
    options.add_argument("--disable-infobars")
    options.add_argument("--mute-audio")
    try:
        driver = webdriver.Chrome(
            executable_path="./chromedriver.exe", options=options)
    except Exception as e:
        print(e)
    
    driver.get("https://online.tableau.com/")
    driver.maximize_window()      

@exc('')
def login(login: str, password: str):
    driver.find_element_by_id("email").send_keys(login)
    driver.find_element_by_id("password").send_keys(password)
    driver.find_element_by_id("login-submit").click()

@exc('')
def open_workbooks():
    wait()
    
    (driver.find_elements_by_xpath("//a[@class='Navigation_nav-link-value_fshr652']"))[0].click()
    
    wait()
    
    url = driver.current_url
    splited_url = url.split("/")
    new_url = '/'.join(splited_url[0:(len(splited_url)-1)]) + '/workbooks'    
    
    driver.get(new_url)

@exc('')
def download_workbooks():
    wait()
    a_links = driver.find_elements_by_xpath("//a[@class='CardView_thumbnail-preloaded_f1vpb8ks']")
    new_links = []
    for link in a_links:
        new_links.append(link.get_attribute("href") + "/views")
    
    if len(new_links) < 10:
        driver.close()
        print("Not enough workbooks")
        exit()
        
    for i in range(min(10, len(new_links))):
        driver.get(new_links[i])
        get_all_sheets()
    
    
exc('') 
def get_all_sheets():
    wait()
    a_links = driver.find_elements_by_xpath("//a[@class='CardView_thumbnail-preloaded_f1vpb8ks']")[0].click()
    auto_gui_clicks_events()
    
def auto_gui_clicks_events():
    # Click download button
    wait()
    pyautogui.moveTo(1680, 140)
    pyautogui.click()
    # Click PDF
    wait()
    pyautogui.moveTo(950, 610)
    pyautogui.click()
    #Click "This View"
    wait()
    pyautogui.moveTo(955, 515)
    pyautogui.click()
    #Click "Specific sheets from this workbook"
    wait()
    pyautogui.moveTo(955, 570)
    pyautogui.click()
    #Click "Select All"
    wait()
    pyautogui.moveTo(949, 580)
    pyautogui.click()
    #Click "Create PDF"
    wait()
    pyautogui.moveTo(1055, 750)
    pyautogui.click()
    #Click "Download"
    wait()
    pyautogui.moveTo(1000, 620)
    pyautogui.click()
    #Close Popup
    wait()
    pyautogui.moveTo(1900, 1015)
    pyautogui.click()
 
    
@exc('')
def convert_to_zip():
    files = [f for f in os.listdir("./reports") if os.path.isfile(os.path.join("./reports", f))]
    with zipfile.ZipFile("report.zip", 'w') as report_zip:
        for f in files:   
            report_zip.write(f"./reports/{f}")
    

@exc('')
def send_to_email(email1, password, email2):
    zf = open('report.zip','rb')
    themsg = MIMEMultipart()
    themsg['Subject'] = '<blank>'
    themsg['To'] = email2
    themsg['From'] = email1
    themsg.preamble = '<blank>'
    msg = MIMEBase('application', 'zip')
    msg.set_payload(zf.read())
    encoders.encode_base64(msg)
    msg.add_header('Content-Disposition', 'attachment', filename='report.zip')
    themsg.attach(msg)
    themsg = themsg.as_string() 
    
    s = smtplib.SMTP('smtp.gmail.com', 587) 
    s.starttls() 
    s.login(email1, password) 
    text = msg.as_string() 
    s.sendmail(email1, email2, text) 
    s.quit() 


def wait():
    time.sleep(5)


def close():
    try:
        driver.close()
        exit()
    except:
        exit()
