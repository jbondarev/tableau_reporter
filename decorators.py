def exc(value):
    def decorator(f):
        def applicator(*args, **kwargs):
            try:
                f(*args,**kwargs)
            except Exception as e:
                print(e)
        return applicator
    return decorator