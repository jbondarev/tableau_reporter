import argparse
from commands import *


def main():
   #argparse
   
   parser = argparse.ArgumentParser()
   parser.add_argument('-p1', '--password1', type=str)
   parser.add_argument('-e1', '--email1', type=str)
   parser.add_argument('-p2', '--password2', type=str)
   parser.add_argument('-e2', '--email2', type=str)
   
   args = parser.parse_args()
   password1 = args.password1
   email1 = args.email1
   password2 = args.password2
   email2 = args.email2
   
   # selenium
   init()
   login(email1, password1)
   open_workbooks()
   download_workbooks()
   convert_to_zip()
   send_to_email(email1, password2, email2)
   close()
   
if __name__ == "__main__":
    main()